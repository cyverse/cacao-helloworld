# CACAO Helloworld

This is the simplest CACAO Hello World example. It is just a simple Hello World example python web application with an Nginx proxy.

This demonstrates CACAO's ability to work with multiple containers in a Pod and route to an HTTP service. It also shows using CACAO with an image not built from the same repository
